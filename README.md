# btrfs-soc
btrfs snap on change (soc) is yet another dumb btrfs snapshot script which noone but me will use.

It does
- create a snapshot on a certain path
- verify if there's an older existing snapshot following the same scheme
	- if so than guess if there are differences between old and new snapshot
	- if not delete new snapshot

# system.d timer

There is a service as well as timer definition for system.d to verify the subvolume every 2 hours.

# install
```
sudo make
```
# install + enable
```
sudo make all
```

# uninstall
```
sudo make uninstall
```
