#!/bin/sh
# this is very dumb script which does try to make a snapshot of $1 with a prefix given in $2.
# if it succeeds than it checks if there a already existing snapshots with given prefix and
# guess via btrfs send and receive if there are changes. If not than it will delete that
# new snapshot.
# - path could contain double slash
# - it assumes that snapshots are done within the same subvolume
# - not general purpose
# - it guesses changes based on the lines of send and receive --dump; not very sophisticated.
# - it fails only when btrfs subvolume snapshot fails, not when unable to check

set -ex
[ -z $1 ] && path="/mnt/storage" || path=$1
[ -z $2 ] && prefix=".snap" || prefix=$2
path="$path/"
last_snapshot=$path$(btrfs subvolume list -s $path | sort -rk14 | awk "\$14 ~ /^$prefix/ {print \$14; exit}")
new_snapshot="$path$prefix$(date +%FT%H%M%S)"
btrfs subvolume snapshot -r "$path" "$new_snapshot"
[ $last_snapshot = $path ] && exit 0

count=$(btrfs send --no-data -p "$last_snapshot" "$new_snapshot" | btrfs receive --dump | wc -l)
[ $count -le 2 ] && btrfs subvolume delete $new_snapshot
