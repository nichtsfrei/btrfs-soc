ifndef DESTINATION
	DESTINATION=/usr/bin
endif
ifndef SNAP_PATH
	SNAP_PATH=/mnt/storage
endif


ifndef OWNER
	OWNER = root
endif

install:
	cp btrfs-soc.sh $(DESTINATION)/
	chown $(OWNER) $(DESTINATION)/btrfs-soc.sh
	chmod 744 $(DESTINATION)/btrfs-soc.sh
	sed -i 's|/mnt/storage|$(SNAP_PATH)|' btrfs-soc.service 
	sed -i 's|/usr/bin|$(DESTINATION)|' btrfs-soc.service 
	cp btrfs-soc.service /etc/systemd/system/
	cp btrfs-soc.timer /etc/systemd/system/
	chown $(OWNER) /etc/systemd/system/btrfs-soc.service
	chown $(OWNER) /etc/systemd/system/btrfs-soc.timer

uninstall:
	rm -f $(DESTINATION)/btrfs-soc.sh
	systemctl disable btrfs-soc.timer || echo "ignoring non existing services"
	systemctl stop btrfs-soc.timer || echo "ignoring non existing services"
	rm -f /etc/systemd/system/btrfs-soc.timer
	rm -f /etc/systemd/system/btrfs-soc.service

enable:
	systemctl enable btrfs-soc.timer
	systemctl start btrfs-soc.timer

all: uninstall install enable
